// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});



$('.review-slider').slick({
    arrows: false,
    autoplay: false,
    dots: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

$('.review-slider-next').click(function(){
    $('.review-slider').slick('slickNext');
});


// Slider

$('.review-source-slider').slick({
    arrows: false,
    autoplay: false,
    dots: true,
    mobileFirst: true,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 768,
            settings: {
                dots: false,
                arrows: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
                nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>'
            }
        }
    ]
});


$('.example-more').click(function(){
    $('.example-hide').show();
    $('.example-more').hide();
});


//  Modal

$(".btn-modal").fancybox({
    'padding'    : 0,
    'maxWidth'   : 1000,
    'tpl'        : {
        closeBtn : '<a title="Close" class="btn-close" href="javascript:;"><i class="fa fa-close"></i></a>'
    }
});


$('.side-toggle a').dropdown();